import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/pages/Login';
import PersonalRental from '@/pages/PersonalRental';
import GroupRental from '@/pages/GroupRental';
import Equipment from '@/pages/Equipment';
import Statistics from '@/pages/Statistics';
import Users from '@/pages/Users';
import Profile from '@/pages/Profile';
import { requireAuth } from '../utils/auth';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      meta: {
        title: 'Login',
        layout: 'blank',
      },
      component: Login,
    },
    {
      path: '/',
      redirect: '/admin/personal-rental',
    },
    {
      name: 'Admin.PersonalRental',
      path: '/admin/personal-rental',
      meta: {
        title: 'PersonalRental',
        layout: 'admin',
      },
      component: PersonalRental,
      beforeEnter: requireAuth,
    },
    {
      name: 'Admin.GroupRental',
      path: '/admin/group-rental',
      meta: {
        title: 'GroupRental',
        layout: 'admin',
      },
      component: GroupRental,
      beforeEnter: requireAuth,
    },
    {
      name: 'Admin.Equipment',
      path: '/admin/equipment',
      meta: {
        title: 'Equipment',
        layout: 'admin',
      },
      component: Equipment,
      beforeEnter: requireAuth,
    },
    {
      name: 'Admin.Statistics',
      path: '/admin/report',
      meta: {
        title: 'Statistics',
        layout: 'admin',
      },
      component: Statistics,
      beforeEnter: requireAuth,
    },
    {
      name: 'Admin.Users',
      path: '/admin/users',
      meta: {
        title: 'Users',
        layout: 'admin',
      },
      component: Users,
      beforeEnter: requireAuth,
    },
    {
      name: 'Admin.Profile',
      path: '/admin/profile',
      meta: {
        title: 'Profile',
        layout: 'admin',
      },
      component: Profile,
      beforeEnter: requireAuth,
    },
  ],
});
