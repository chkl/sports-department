import router from '../router/index';

let loggedIn = false;

export function login() {
  loggedIn = true;
  router.push({ name: 'Admin.PersonalRental' });
}

export function confirmPasswordReset() {
}

export function resetPassword() {
}

export function isLoggedIn() {
  return loggedIn;
}

export function logout() {
  loggedIn = false;
  router.push({ name: 'Login' });
}

export function requireAuth(to, from, next) {
  if (!isLoggedIn()) {
    router.push({ name: 'Login' });
  } else {
    next();
  }
}
